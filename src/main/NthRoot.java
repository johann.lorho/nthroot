package main;

public class NthRoot {

	/*
	 * Calculates the n-th root of a number with a certain precision
	 */
	public static double nthRoot(double number, int root, double precision) {
		if (number < 1 || root < 1) {
			throw new IllegalArgumentException("Expected : number and root should be greater or equal to 1.");
		}
		if (precision <= 0) {
			throw new IllegalArgumentException("Expected : precision should be strictly greater than 0.");
		}

		double guess = 1; // initial estimate of the root
		double error = number; // initial error between estimate and target
		int c = 0; // counting iterations
		// iterate until error is small enough
		while (error > precision) {
			double numerator = number;
			for (int i = 1; i < root; i++) {
				numerator = numerator / guess;
			}
			double newGuess = ((root - 1.0) * guess + numerator) / root;
			error = absoluteValue(newGuess - guess);
			guess = newGuess;
			c++;
		}
		System.out.println("After " + c + " iterations, the " + root + "th root of " + number + " is " + guess);
		return guess;
	}

	/*
	 * Same thing, but precision is set to 0.000000001 by default
	 */
	public static double nthRoot(double x, int n) {
		double precision = 0.000000001;
		return nthRoot(x, n, precision);
	}

	/*
	 * Calculates number's absolute value without using Math library
	 */
	public static double absoluteValue(double number) {
		long bits = Double.doubleToLongBits(number);
		bits &= 0x7fffffffffffffffL;
		return Double.longBitsToDouble(bits);
	}

	/*
	 * Main method
	 */
	public static void main(String[] args) {
		double precision = 0.000000001;
		nthRoot(56136548764865., 9, precision);
	}
}