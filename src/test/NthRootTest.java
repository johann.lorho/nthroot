package test;

import java.util.Random;

// JUnit 5
import org.junit.Assert;
import org.junit.Test;

import main.NthRoot;

public class NthRootTest {

	/*
	 * Tests that number < 1 raises an exception
	 */
	@Test
	public void testIllegalArgNumber() {
		boolean pass = false;
		try {
			NthRoot.nthRoot(0, 1);
		} catch (IllegalArgumentException e) {
			pass = true;
		}
		Assert.assertTrue(pass);
	}

	/*
	 * Tests that root < 1 raises an exception
	 */
	@Test
	public void testIllegalArgRoot() {
		boolean pass = false;
		try {
			NthRoot.nthRoot(1, 0);
		} catch (IllegalArgumentException e) {
			pass = true;
		}
		Assert.assertTrue(pass);
	}

	/*
	 * Tests that precision < 0 raises an exception
	 */
	@Test
	public void testIllegalArgPrecision() {
		boolean pass = false;
		try {
			NthRoot.nthRoot(1, 1, 0);
		} catch (IllegalArgumentException e) {
			pass = true;
		}
		Assert.assertTrue(pass);
	}

	/*
	 * Tests 100 random values of number and root Compares to the value obtained
	 * using Math.pow Asserts that difference < precision
	 */
	@Test
	public void testRandomValues() {
		double precision = 0.000000001;
		Random random = new Random();
		for (int d = 0; d < 100; d++) {
			double number = 1 + random.nextDouble(100000000);
			int root = 1 + random.nextInt(1000);
			System.out.println(String.format("Tested with number = %s, root = %s", number, root));
			double a = NthRoot.nthRoot(number, root, precision);
			double b = Math.pow(number, 1. / root);
			double diff = Math.abs(a - b);
			System.out.println("Difference = " + diff);
			System.out.println();
			Assert.assertTrue(diff <= precision);
		}
	}
}
